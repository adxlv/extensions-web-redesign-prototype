# Page layout

IN PROGRESS

`.page-layout` provides common page layout structure, supporting the following children:

- header
- nav
- main

Basic example:

```html
<div class="page-layout">
    <header>
        <!-- header -->
    </header>

    <nav>
        <!-- nav sidebar -->
    </nav>

    <main>
        <!-- main content -->
    </main>
</div>
```

You may use semantic tags directly (`header`, `nav`, `main`), or reference them with a `data-child` attribute. Example: `<div data-child="main">`.

## Narrow viewport display

On narrow viewports, the `data-narrow-view` attribute determines whether only `nav` or `main` columns will be visible.

`data-narrow-view` options:
- `all` (default)
- `nav`
- `main`

`header` is always visible. (and `footer`?)

<!--
`data-narrow-view="nav | main"`
`data-default-region="nav"`
`data-narrow-policy="show-nav" /* show-nav | show-main | auto`
-->

### Target visibility

When a URL fragment points to the `id` of a page layout child (example: `index.html#main`), the targetted section will be visible.

This is useful especially on narrow viewports, so a previously hidden column can be made visible for the user.

List-detail example:

```html
<div class="page-layout">
    <nav>
        <ul class="nav-list">
            <li><a href="index.html#main">List item 1</a></li>
            <li><a href="two.html">List item 2</a></li>
            <li><a href="three.html">List item 3</a></li>
        </ul>
    </nav>

    <main id="main">
        Page detail 1
    </main>
</div>
```

## Conditional visibility

`data-hidden-condition`:
- `page-layout:nav-only`
- `page-layout:nav-shown`
- `page-layout:main-only`
- `page-layout:main-shown`

<!--

`hide-on-main-view`
`hide-on-`

`hide-when-narrow`

```html
<div data-hidden="current-region-is-main">
```

`data-hidden-condition="page-layout:nav-only"`
`data-hidden-condition="page-layout:main-only"`

`data-hidden-condition="page-layout:nav-shown"`
`data-hidden-condition="page-layout:main-shown"`

`data-hidden-condition="viewport:narrow"`

-->