# Responsive design

## Viewport ranges

Two viewport ranges:
- `narrow` (width <= 768px)
- `regular` (width > 768px)

Narrow viewports target mobile-like experiences, where a single layout column can fit the space.

How to target viewports ranges in CSS:

```css
@media (width <= 768px) {
    /* narrow viewport */
}

@media (width > 768px) {
    /* regular viewport */
}
```

(This could be upgraded to [CSS custom media queries](https://www.w3.org/TR/mediaqueries-5/#custom-mq) once there's browser support)


## Conditional visibility

To hide parts of a page depending on the viewport range, use the `data-hidden-condition` attribute:

```html
<div class="card" data-hidden-condition="viewport:narrow">
    This card is hidden on narrow viewports.
</div>

<div class="card" data-hidden-condition="viewport:regular">
    This card is hidden on regular viewports.
</div>
```