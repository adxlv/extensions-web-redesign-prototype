const layoutAliases = [
	'base',
	'minimal',
	'default',
	'_internal-doc',
];

const staticPaths = {
	'src/assets': '/assets',
	'src/scripts': '/scripts',
	'src/styles': '/styles',
	'src/favicon.svg': '/favicon.svg',
};

module.exports = function(eleventyConfig) {

	/* passthrough copies */
	for (const url in staticPaths) {
		eleventyConfig.addPassthroughCopy({[url]: staticPaths[url]});
	}

	/* layout aliases */
	for (const alias of layoutAliases) {
		eleventyConfig.addLayoutAlias(alias, `${alias}.njk`);
	}

	return {
		htmlTemplateEngine: "njk",
		dir: {
			input: "src/views",
			includes: "_includes", /* relative to input dir */
			data: "_data",
			layouts: "_layouts",
			output: "_site",
		}
	}
};