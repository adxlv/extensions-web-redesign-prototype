## A redesign exploration for extensions.gnome.org

**⚠ this exploration is no longer being worked on. See https://gitlab.gnome.org/adxlv/web-patterns for spin-off of this.**

---

This repository contains an in-progress frontend prototype (HTML/CSS/JS), exploring a redesign for the [GNOME Extensions](https://extensions.gnome.org) website.

The redesign exploration is considering a design language that is heavily inspired by [Adwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/) and the [GNOME HIG](https://developer.gnome.org/hig/), with reusable patterns and components that could also be used by other GNOME web experiences.

This frontend aims to be platform-agnostic, with only [responsive/semantic HTML](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Responsive_Design), [preprocessor-free CSS](https://developer.mozilla.org/en-US/docs/Glossary/CSS_preprocessor), and unobstrusive Javascript. This way it could be adopted by any architecture, or retrofitted to the current [`extensions-web`](https://gitlab.gnome.org/Infrastructure/extensions-web/) codebase.

## Develop

This repository uses [`eleventy`](https://www.11ty.dev/) to parse basic template functionality (such as `{% include 'main-nav.html' %}`), with the [`nunjucks`](https://mozilla.github.io/nunjucks/) language.

## Running

Make sure you have `npm` >= 14.

- Run: `npm run start`
- Build `npm run build` (outputs to `_site` directory)

## Mocks

**⚠️ IN PROGRESS**

### List view
[![List view](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/raw/main/_mockups/list-view.png?v2)](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/blob/main/_mockups/list-view.png)

### Detail view, public
[![Detail view, public](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/raw/main/_mockups/extension-detail-view-public.png)](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/blob/main/_mockups/extension-detail-view-public.png)

### Detail view, author
[![Detail view, author](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/raw/main/_mockups/extension-detail-view-author.png)](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/blob/main/_mockups/extension-detail-view-author.png)

### Extension management
[![Extension management](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/raw/main/_mockups/extension-management.png)](https://gitlab.gnome.org/adxlv/extensions-web-redesign-prototype/-/blob/main/_mockups/extension-management.png)

## Progress

_For now, a rough list_.

- [x] Common elements
    - [x] Top navigation
    - [x] Content wrappers
    - [ ] Log in/sign up popover
- [ ] List view
    - [ ] Search
        - [x] Search by text
        - [ ] Sort results
        - [ ] Show only compatible extensions
    - [x] List items
    - [x] Pagination
- [ ] Detail view
    - [ ] Extension header
        - [x] Icon, title, subtitle
        - [ ] Extension actions
            - [ ] Install action
            - [ ] Turn off/on toggle
            - [ ] Settings action
            - [ ] Update action
            - [ ] Remove action
    - [ ] Extension description
        - [x] Rich content
        - [ ] Screenshot area
    - [ ] Extension metadata
        - [x] Project website + generic `<dl>` support
        - [x] # of Downloads
    - [ ] Extension actions
        - [x] Download button
        - [ ] Download popover
        - [x] Donate button
        - [x] Report a bug button
        - [x] Write a comment button
    - [x] Reviews/comments
        - [x] Basic comment rendering
- [ ] Installed extensions page
    - [ ] Global actions
        - [ ] Disable all extensions
        - [ ] Disable version validation
    - [ ] Extension actions
    - [ ] System extension indicator
    - [ ] Outdated state
- [ ] User page


Other known tasks/features

- [ ] Icon library cleanup
- [ ] Card for "No browser extension found"
- [ ] Dark mode
- [ ] Browser extension redesign
- [ ] Other interstitial pages

---

## References

Some links I've saved along the way to start this exploration:

- [The GNOME Extensions Rebooted Initiative](https://blogs.gnome.org/sri/2020/09/16/the-gnome-extensions-rebooted-initiative/), by @sri (2020-09)
    - [BoF at Guadec 2020](https://www.youtube.com/watch?v=pC5im1QDNKI)
    - [`extensions-rebooted` project](https://gitlab.gnome.org/World/ShellExtensions/extensions-rebooted)

- ["Rethinking extensions.gnome.org"](https://gitlab.gnome.org/Infrastructure/extensions-web/-/issues/116) issue, by @nE0sIghT (2020-03)
- [extensions.gnome.org: yesterday, today and tomorrow](https://blogs.gnome.org/ne0sight/2017/01/21/extensions-gnome-org-yesterday-today-and-tomorrow/), by @nE0sIghT (2017-01)

- [Extensions.gnome.org design refresh](https://discourse.gnome.org/t/extensions-gnome-org-design-refresh/14719/2) by @oilipheist, @andyholmes (2023-03)
