const styles = new CSSStyleSheet();
styles.replaceSync(/*css*/`
    :host {
        box-shadow: 0 4px 10px red;
        border: 1px solid red;
        inset-inline-start: 60px !important;
    }
`);

const template = document.createElement("template");
template.innerHTML = /*html*/`
    <div class="popover-content">
        <div>
            <h4>GNOME version</h4>
            <div>
                <slot name="gnome-version"></slot>
            </div>
        </div>
        <div>
            <h4>Extension version</h4>
            <div>
                <slot name="extension-version"></slot>
            </div>
        </div>
    </div>
`;

export class DownloadPopoverElement extends HTMLElement {
    static define(tag = "download-popover") {
        if (window.customElements.get(tag)) return;
        customElements.define(tag, this);
    }

    shadowRoot = this.attachShadow({ mode: 'open' });

    /* private fields */
    #downloadButton;

    /* public fields */

    // #region [ Custom element API ]

    connectedCallback() {
        this.#prepareDom();
    }

    // #endregion

    updateAnchorPosition() {
        const position = this.#downloadButton.getBoundingClientRect();
        console.log(position);
    }

    #prepareDom() {
        this.#prepareTemplate();

        // fixme: get id from popovertarget
        this.#downloadButton = document.querySelector('#download-button');

        this.#downloadButton.onclick = this.updateAnchorPosition.bind(this);
    }

    #prepareTemplate() {
        this.shadowRoot.adoptedStyleSheets = [styles];
		const internalNodes = template.content.cloneNode(true);
		this.shadowRoot.replaceChildren(internalNodes);
    }
}

DownloadPopoverElement.define();