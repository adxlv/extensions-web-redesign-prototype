// GTK-style popover behavior

// Uses the `popover` attribute in HTML to make a GNOME-style popover,
// with centered positioning and arrow.

// Uses the Popper[1] library as a positioning engine.

// Reference:
// On HTML popover attribute: https://hidde.blog/popover-semantics/
// [1]: https://popper.js.org/

// Todo: Add support for <dialog>, which blocks the rest of the page while interacting.
// Todo: Add support for menu-style keyboard interactions

document.addEventListener("DOMContentLoaded", (event) => {

    const popoverTriggerButtons = document.querySelectorAll('button[popovertarget]');
    
    popoverTriggerButtons.forEach((button) => {
        console.log(button);
        const target = button.getAttribute('popovertarget')
        const targetElement = document.getElementById(target);

        // add popper arrow markup
        const arrowElement = document.createElement('div');
        arrowElement.setAttribute('data-popper-arrow', 'true');
        targetElement.prepend(arrowElement);

        // create popper instance
        const popperInstance = Popper.createPopper(button, targetElement, {
            modifiers: [
                { name: 'eventListeners', enabled: true },
                { name: 'arrow', options: { padding: 0 }},
                { name: 'offset', options: { offset: [0, 20] }},
                { name: 'preventOverflow', options: { padding: 8 }},
            ]
        });

        //enableEventListeners(false);

        targetElement.addEventListener('toggle', ({ currentState, newState }) => {
            const enableEventListeners = (newState === 'open') ? true : false;
            updatePopperEventListener(popperInstance, enableEventListeners);
        });
    });

    const updatePopperEventListener = (popperInstance, newValue) => {
        popperInstance.setOptions((options) => ({
            ...options,
            modifiers: [
                ...options.modifiers,
                { name: 'eventListeners', enabled: newValue},
            ],
        }));
        popperInstance.update();
    };
});