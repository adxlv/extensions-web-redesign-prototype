---
title: Site Map
layout: _internal-doc
---

# GNOME Extensions Web Prototype

Lorem ipsum dolor.

## Core functionality

- [List page (landing page)](/)
- [Extension detail page](/extensions/detail/)
    - [All versions](/extensions/versions/)
- [Installed extensions](/installed/)
- [Profile page](/profile/)

## Static pages

- [About page](/about/)

## Logged-in pages

- [Manage extension](/manage-extension/)
    - [Extension details](/manage-extension/)
    - [Versions](/manage-extension/versions/)

## UI Patterns

- UI Patterns index
- [Main header](/_ui-patterns/main-header/)
- Main footer
- Rich content